#include<stdio.h>
void search();
int main()
{
  printf("Program to find an element in an array.\n"); 
  search();
  return 0;
}
void search()
{
  int a[25],i,n,ele,found=0;
  printf("How many elements ?\n");
  scanf("%d",&n);
  printf("Enter the elements one after another.\n");
  for(i=0;i<n;i++) 
  {
     scanf("%d",&a[i]);
  }
  printf("Enter the element to be found\n");
  scanf("%d",&ele);
  for(i=0;i<n;i++)
  {
      if(a[i]==ele)
      {
          found=1;
          printf("Element found\n");
          printf("Position of the element in array is : %d",i+1);
          break;
     }
  }
  if(found==0)
  {
      printf("Element not found");
  }
}