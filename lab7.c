#include<stdio.h>
void perform();
int main()
{
    printf("Program to insert an element from the array.\n");
    perform();
    return 0;
}
void perform()
{   
    int a[6]={2,3,5,11,13};
    int i,pos,ele;
    printf("The elements in the array are \n");
    for(i=0;i<5;i++)
    {
        printf("%d\t",a[i]);
    }
    printf("\nEnter the element to be inserted in the array.\n");
    scanf("%d",&ele);
    printf("Enter the position of the element to be deleted in the array.\n");
    scanf("%d",&pos);
    for(i=5;i>=pos-1;i--)
    {
        a[i+1]=a[i];
        
    }
    a[pos-1]=ele;
    printf("The elements in the array after inserting the number.\n");
    for(i=0;i<=5;i++)
    {
        printf("%d\t",a[i]);
    }
}