#include<stdio.h>
void perform();
int main()
{
    printf("Program to delete an element from the array.\n");
    perform();
    return 0;
}
void perform()
{   
    int a[6]={2,4,5,6,8,10};
    int i,pos,ele;
    printf("The elements in the array are \n");
    for(i=0;i<=5;i++)
    {
        printf("%d\t",a[i]);
    }
    printf("\nEnter the element to be deleted in the array.\n");
    scanf("%d",&ele);
    printf("Enter the position of the element to be deleted in the array.\n");
    scanf("%d",&pos);
    for(i=pos;i<=5;i++)
    {
        a[i-1]=a[i];
        
    }
    printf("The elements in the array after removing the number.\n");
    for(i=0;i<5;i++)
    {
        printf("%d\t",a[i]);
    }
}