//READ 2D ARRAY OF 2 STUDENTS
#include<stdio.h>
void student();
int main()
{
    printf("Program to display highest marks in each subject.\n");
    student();
    return 0;
}
void student()
{
    int marks[5][3];
    int i,j,high;
    for(i=0;i<5;i++)
    {
        for(j=0;j<3;j++)
        {
            printf("Enter the marks of student %d in subject %d:",i+1,j+1);
            scanf("%d",&marks[i][j]);
        }
    }
   printf("The marks list is as follows.\n");
   
   for(i=0;i<5;i++)
   {  
       printf("Student %d\n",i+1);
       for(j=0;j<3;j++)
       {
          printf("Subject %d = %d\t",j+1,marks[i][j]); 
       }
       printf("\n");
   }
   for(i=0;i<3;i++)
   {
       high=0;
       for(j=0;j<5;j++)
       { 
         if(high<marks[j][i]) 
          {
              high=marks[j][i];
          }
       }
       printf("The highest marks in subject %d is : %d\n",i+1,high);
   }
}