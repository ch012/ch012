//ADD 2 2D ARRAY
#include<stdio.h>
void sum();
int main()
{
    printf("Program to find the sum of two  2D arrays.\n");
    sum();
    return 0;
}
void sum()
{
    int i,j,r,c;
    int a[5][5],b[5][5],s[5][5];
    printf("Enter the number of rows in the arrays.\n");
    scanf("%d",&r);
    printf("Enter the number of columns in the arrays.\n");
    scanf("%d",&c);
    for(i=0;i<r;i++)
     {
        for(j=0;j<c;j++)
         {
           printf("Enter the elements of a[%d][%d] = ",i,j);
           scanf("%d",&a[i][j]);
         }
     }
      printf("\n");
    for(i=0;i<r;i++)
     {
        for(j=0;j<c;j++)
         {
           printf("Enter the elements of b[%d][%d] = ",i,j);
           scanf("%d",&b[i][j]);
         }
     }
    for(i=0;i<r;i++)
     {
        for(j=0;j<c;j++)
         {
           s[i][j]=a[i][j]+b[i][j];
         }
     }
    printf("\n"); 
    printf("Array 1 is as follows:\n"); 
    for(i=0;i<r;i++)
     {
        for(j=0;j<c;j++)
         {
            printf("%d\t",a[i][j]);
         }
        printf("\n");
     }
    printf("Array 2 is as follows:\n"); 
    for(i=0;i<r;i++)
     {
        for(j=0;j<c;j++)
         {
            printf("%d\t",b[i][j]);
         }
        printf("\n");
     }
    printf("Array with sum of above two arrays.\n"); 
     for(i=0;i<r;i++)
      {
        for(j=0;j<c;j++)
         {
            printf("%d\t",s[i][j]);
         }
        printf("\n");
      }
}